﻿namespace SystemManagement.Truck.Domain.Entities
{
    public class Truck
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public string Code { get; set; } = string.Empty;

        public TruckStatus Status { get; set; } = default!;
    }
}
